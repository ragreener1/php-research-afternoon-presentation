\documentclass[aspectratio=169]{beamer}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\usepackage{hyperref}
\usepackage[authoryear,round]{natbib}
\bibliographystyle{plainnat}
\usepackage[default]{sourcesanspro}
\usepackage{fontawesome5}
\usepackage{listings}

\title[MOTIVATE]{Incorporating social norms into a configurable agent-based model of commuting behaviour in London: methodological insights for modelling complex systems}
\author[Robert Greener]{Robert Greener \and Daniel Lewis \and Jon Reades \and Simon Miles \and Steven Cummins}
\institute[LSHTM]{London School of Hygiene and Tropical Medicine}
\date[28th June 2022\quad \faCreativeCommons\ \faCreativeCommonsBy\ \faCreativeCommonsSa]{28th June 2022}
\titlegraphic{\includegraphics[height=3em]{philab.png}\quad\includegraphics[height=3em]{LSHTM-logo-bw.jpg}\quad\includegraphics[height=3em]{Medical_Research_Council_logo.svg.png}}

\usetheme{Madrid}

\begin{document}
\maketitle

\section{Background}

\begin{frame}{Background}
    \begin{itemize}
        \item There is a growing commitment in the UK and worldwide to rebalancing the travel system, so that more journeys are made using more active modes, such as walking and cycling.
        \item Walking and cycling are good for people's physical and mental health, but also, they reduce dependency on motorized transport, particularly cars, which will reduce noise and air pollution, reduce the risk of road traffic injury, and help prevent cardiovascular disease and obesity.
        \item Therefore, there are significant population health gains to be made by moving people into more active modes of travel.
        \item Social norms as well as system-based approaches are understudied in public health. However, these could be key to understanding what makes an intervention successful.
    \end{itemize}
\end{frame}

\begin{frame}{Model aims}
    \begin{itemize}
        \item \emph{Research question}: What social norms related to commuting behaviour arise from modelling the decision process of commuting behaviour?
        \item The aim is to build a configurable agent-based model of the \emph{decision} to perform commuting behaviour.
        \item This means that the actual process of commuting is not modelled -- we don't model how traffic flows through cities.
        \item We look at the higher-level mode-choice decision rather than the act of travel.
        \item We aim to include the effect of the actions of peers and neighbours, subculture, habit, weather, bicycle ownership, car ownership, environmental supportiveness, and congestion on the decision to travel by walking, cycling, driving, or public transport.
        \item All the parameters are configurable allowing for other users to calibrate the model to their own area of interest.
    \end{itemize}
\end{frame}

\section{Methods}
\begin{frame}{What is an agent-based model?}
    \begin{itemize}
        \item Agent-based simulation is a bottom-up modelling process in which agents -- individuals or households in the context of MOTIVATE -- are imbued with simple behavioural rules that define how they act in an environment and interact with one another \citep{filatovaSpatialAgentbasedModels2013}.
        \item An ABM is designed to explore \emph{emergence} -- the whole is greater than the sum of its parts.
        \item An ABM is a type of complex-systems model.
        \item An ABM can be useful as a decision-support tool by modelling various policy scenarios cheaply, particularly where there is an absence of high-grade real-world evidence.
    \end{itemize}
\end{frame}

\begin{frame}{Some Assumptions}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item 4 options at every time step: walk, cycle, public transport, or drive.
                \item Modelled loosely on Waltham Forest, a North Eastern London Borough.
                \item Used a microsimulation approach to ensure wards (neighbourhoods) matched real life on things such as car ownership and commute distance. This used data from UK Household Longitudinal Survey \citep{universityofessexUnderstandingSocietyWaves2020} and 2011 Census population data \citep{officefornationalstatistics2011CensusAggregate2017}.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{neighbourhood-grid.pdf}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Environmental assumptions}
    \begin{itemize}
        \item Each agent live in a neighbourhood.
              \begin{itemize}
                  \item Neighbourhoods have a \emph{supportiveness for active travel} score.
                  \item Neighbourhoods also have a physical capacity for given modes which once passed results in \emph{congestion}, a negative influence.
                  \item There is variation in neighbourhood supportiveness between neighbourhoods.
              \end{itemize}
        \item Supportiveness is a \emph{composite} configurable parameter.
        \item We can change the score to reflect positive or negative changes in the supportiveness of the environment.
              \begin{itemize}
                  \item Environmental changes are \emph{low-frequency}, potentially working through norms.
              \end{itemize}
        \item We also allow for weather as a factor affecting daily mode choice.
              \begin{itemize}
                  \item Weather changes are \emph{high-frequency}, imposing mode choice.
              \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Agent assumptions}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Agents norms arise through a combination of:
                      \begin{itemize}
                          \item their friends;
                          \item their neighbours; and
                          \item their mobility culture.
                      \end{itemize}
                \item A mobility culture could be seen as the process of creation and maintenance of allegiance to particular forms of mobility.
                \item The main task of an agent is to commute to work with a travel mode.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{agent-belongings.pdf}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{A model run}
    \begin{itemize}
        \item Each agent goes to work every weekday.
        \item Each weekday, each agent calculates a \emph{budget} of how much they are willing to commute by a given mode (norm + habit + congestion).
        \item They also calculate a cost for each mode (distance + environment supportiveness + weather)
        \item Max of budget \textminus{} cost.
    \end{itemize}
\end{frame}

\section{Experimentation}
\begin{frame}{Modelling car-free days}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item In order to demonstrate the utility of the model, we implemented an intervention where cars were banned on Wednesdays.
                \item After year 1, cars were banned. The simulation were run for 4 more years.
                \item This introduced greater instability into the journeys taken, but significantly greater number of active commutes.
                \item In the control scenario, over 200 runs, the odds of an active journey were 0.091 (89\% HPDI: [0.091, 0.091]). The odds of an active journey were 77.7\% (89\% HPDI: [77.7\%, 77.7\%]) greater in the car-free days scenario.
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{cfd.png}
                \caption{14 day moving average of active commutes}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{Conclusions and End}
\begin{frame}{Conclusions}
    \begin{itemize}
        \item Thinking about norms and habits are key.
        \item In our sample intervention, habits and norms allow us to destabilize the convention of commuting by car. We disrupt this convention by preventing the effect of habit (on car-free days).
        \item By modelling emergent social norms, we can explore how long-term change occurs.
        \item By allowing many runs of the simulation, precise estimates of the effects within the model can be attained.
        \item Further work could extend our model by calibrating the built environment data to see how the environment interacts with social norms.
        \item If the system your modelling is complex, to truly understand it, complex system methods are required.
    \end{itemize}
\end{frame}

\begin{frame}{End}
    \begin{itemize}
        \item Pre-print available on arXiv:2022.11149 (\url{https://doi.org/hk5x}).
        \item Source code available under a permissive free software licence (Expat) (\url{https://doi.org/hvpd}).
        \item This research was funded by KCL--LSHTM seed funding. Robert Greener is supported by a Medical Research Council Studentship [grant number: MR/N0136638/1]. Steven Cummins is funded by Health Data Research UK (HDR-UK). HDR-UK is an initiative funded by the UK Research and Innovation, Department of Health and Social Care (England) and the devolved administrations, and leading medical research charities.
    \end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{References}
    \footnotesize
    \bibliography{library}
\end{frame}

\end{document}
